class MessagingMiddleware(object):
	def __init__(self, get_response):
		self.get_response = get_response
	def __call__(self, request):
		if hasattr(request, 'user'):
			from messaging.models import get_member_from_request
			member = get_member_from_request(request)
			if member:
				request.member = member
		response = self.get_response(request)
		return response
