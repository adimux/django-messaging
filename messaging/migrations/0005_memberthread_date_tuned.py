# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('messaging', '0004_auto_20160901_2147'),
    ]

    operations = [
        migrations.AddField(
            model_name='memberthread',
            name='date_tuned',
            field=models.DateField(default=datetime.datetime.now),
        ),
    ]
