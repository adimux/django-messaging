from .models import *
from django.db import connection
from core.channels import ChannelHandler


class ThreadHandler(ChannelHandler):
    name = 'threads'
    def get_member(self):
        from .models import Message, Channel, PrivateDiscussion, Member
        if hasattr(self.connection.authenticator, "user") and self.connection.authenticator.user != None:
            sender = Member.objects.get(pk=self.connection.authenticator.user.pk)
        else:
            sender = None
        return sender

    @classmethod
    def form_thread_channel_name(self, thread):
        raise NotImplementedError("Class {} must implement form_thread_channel_name()".format(self.__class__))

    def get_thread(self, channel_name):
        raise NotImplementedError("Class {} must implement get_thread() method".format(self.__class__))

    def handle_channel_message(self, channel, data):
        """
        @return {boolean} : True to publish the same message. False or None to do nothing.
            {dict}    : Another Message to publish
        """
        from .models import Message, Channel, PrivateDiscussion, Member
        
        payload_type = data['type']
        discussion_channel = self.get_thread(channel)
        #if payload_type == "tune":
        #   try:
        #       discussion_channel.join(sender)
        #   except Channel.ExistingMember:
        #       pass
        #   discussion_channel.tune(sender)
        if payload_type == 'tune':
            sender = self.get_member()
            discussion_channel.tune(sender)     
        elif payload_type == 'untune':
            sender = self.get_member()
            discussion_channel.untune(sender)       
        elif payload_type == 'message':
            #sender = Member.objects.get(pk=data["payload"]["sender"]["@pk"])
            sender = self.get_member()
            message_dict = {"content":data["payload"]["content"]}
            print "Message"
            print message_dict
            try:
                discussion_channel.join(sender)
            except Channel.ExistingMember:
                pass
            discussion_channel.tune(sender)
            sent_message = discussion_channel.send(Message(**message_dict), sender)
            
            from .serializers import MessageSerializer
            ser = MessageSerializer(sent_message)
            message_data = ser.data
            
            print "Message data"
            print message_data
            
            self.publish(channel, "message", message_data)
        """
        #Mark the conversation as seen by the user
        """
        #elif payload_type == 'see':
        #if hasattr(self.connection.authenticator, "user") and self.connection.authenticator.user != None:
        #member = Member.objects.get(pk=self.connection.authenticator.user.pk)

class DiscussionChannelHandler(ThreadHandler):
    name = 'channels'

    @classmethod
    def form_thread_channel_name(self, thread):
        return self.form_channel_full_name(thread.slug)
    
    def get_thread(self, channel_name):
        from messaging.models import Channel
        return Channel.objects.get(slug=channel_name)

class PrivateDiscussionHandler(ThreadHandler):
    name = 'privatediscussions'
    @classmethod
    def form_thread_channel_name(self, thread):
        return self.form_channel_full_name( thread.slug )
    def get_thread(self, channel_name):
        from messaging.models import PrivateDiscussion
        return PrivateDiscussion.objects.get(slug=channel_name)


channels = DiscussionChannelHandler
privatediscussions = PrivateDiscussionHandler
