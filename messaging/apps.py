from core.apps import BaseConfig

class AppConfig(BaseConfig):
	name = 'messaging'
	actors = ['Channel', 'PrivateDiscussion', 'Member']
	models_hk_serializers = {'messaging.Channel':'ChannelSerializer', 'messaging.PrivateDiscussion':'PrivateDiscussionSerializer', 'messaging.Member': 'MemberSerializer'}
