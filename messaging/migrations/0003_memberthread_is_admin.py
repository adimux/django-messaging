# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('messaging', '0002_thread_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='memberthread',
            name='is_admin',
            field=models.BooleanField(default=False),
        ),
    ]
