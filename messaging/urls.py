from django.conf.urls import patterns, url
import views
from rest_framework.routers import DefaultRouter
from django.conf.urls import include
from rest_framework_nested import routers as nested_routers
from rest_framework_extensions.routers import ExtendedSimpleRouter

router = ExtendedSimpleRouter()

channel_router = router.register(r'channel', views.ChannelViewSet, base_name='channel')
privatediscussion_router = router.register(r'privatediscussion', views.PrivateDiscussionViewSet)


router.register(r'member', views.MemberHkViewSet)
#router.register(r'message', views
router.register(r'messages', views.MessageViewSet)
router.register(r'references', views.MessageReferenceViewSet)


#channel_router = nested_routers.NestedSimpleRouter(router, r'channel', lookup='thread')
channel_router.register(r'messages', views.MessageViewSet, base_name='channel-messages',  parents_query_lookups=['thread'] )
channel_router.register(r'member', views.MemberHkViewSet, base_name='channel-members',  parents_query_lookups=['thread'] )
channel_router.register(r'member_relations', views.MemberThreadViewSet, base_name='channel-members-relation',  parents_query_lookups=['thread'] )

#privatediscussion_router = nested_routers.NestedSimpleRouter(router, r'privatediscussion', lookup='thread')
privatediscussion_router.register(r'messages', views.MessageViewSet, base_name='privatediscussion-messages',  parents_query_lookups=['thread'] )

urlpatterns = patterns('',
		url(r'^api/', include(router.urls)),
		#url(r'^api/', include(channel_router.urls)),
		#url(r'^api/', include(privatediscussion_router.urls)),
		#url(r'^api/member/(?P<pk>\d+)/$', views.MemberViewSet.as_view({'get':'retrieve'}), name='member-detail' ),
)
	

