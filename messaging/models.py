# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.db.models.signals import pre_save
from django.dispatch import receiver
from datetime import timedelta
from datetime import datetime
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.db.models.signals import post_save
from decimal import *
from swingtime.conf import settings as swingtime_settings
from colorful.fields import RGBColorField
from django.db import models
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
#from django.contrib.auth.models import User
#from install.models import AgfUser
from django.contrib.contenttypes.fields import *
from django.apps import AppConfig
from django.core.urlresolvers import reverse
from django.core.validators import MinValueValidator, MaxValueValidator
from django.core.exceptions import ObjectDoesNotExist
from django.apps import apps
from datetime import *
from swingtime import models as swingtime
from django.conf import settings
from contrat.models import *
from django.core.exceptions import ValidationError
# from core.models import *
from django.contrib.auth import get_user_model
from omnibus.api import publish
from messaging.channels \
    import channels as messaging_channels
from messaging.channels \
    import privatediscussions as privatediscussions_channels
from django.core.exceptions import ObjectDoesNotExist
from django.template.defaultfilters import slugify
import logging
from django.utils import timezone

logger = logging.getLogger(__name__)


def get_member_from_user(user):
    return Member.objects.get(pk=user.pk)


def get_member_from_request(request):
    return get_member_from_user(request.user)

# class MemberManager(models.Manager):
#   def from_user(user):
#       return sefl.get(pk=user.pk)


class Member(get_user_model()):
    class Meta:
        proxy = True

Member.add_to_class('category', get_user_model())


class ThreadManager(models.Manager):
    def unchecked(self, member):
        """
        Returns the unchecked threads.
        """
        return self.find(
            members_relations__member=member,
            checked=False
            ).select_related('thread')

    def threads(self, member):
        return self.find(
            members_relations__member=member
            ).select_related('thread')

    def member_threads(
            self,
            member,
            relation_order=('-members_relations__unchecked_count',)):
        return self.filter(members_relations__member=member) \
                    .order_by(*relation_order)


class UnauthorizedException(Exception):
    pass


class ExistingMemberException(Exception):
    pass


class Thread(models.Model):

    def belongs(self, member):
        return self.members_relations.filter(member=member).count() != 0

    def tune(self, member):
        try:
            relation = self.members_relations.get(member=member)
            relation.tune()
        except ObjectDoesNotExist:
            raise UnauthorizedException(
                "Member %s cannot tune to the thread, "
                + "without first joining it.".format(member))

    def untune(self, member):
        try:
            relation = self.members_relations.get(member=member)
            relation.untune()
        except ObjectDoesNotExist:
            raise UnauthorizedException(
                "Member '{}' cannot untune from the thread since"
                + "he doesn't belog to the thread '{}'.".format(member, self))

    def is_tuned(self, member):
        try:
            relation = self.members_relations.get(member=member)
            relation.is_tuned()
        except ObjectDoesNotExist:
            raise UnauthorizedException(
                "Member '{}' cannot doesn't"
                + " belong to the thread '{}'. Thus, verifying that"
                + " he is tuned or not does not make sense ! Try to"
                + " use thread.join(member) before verifying if the"
                + " member whether the member is tuned or not."
                .format(member, self))

    def see(self, member):
        """
        When a user checks the conversation thread, we want to mark
        the thread as 'seen' (or checked)
        """
        try:
            relation = self.members_relations.get(member=member)
        except:
            raise RuntimeError('Member %s does not belong to the thread.' % member)
        
        relation.checked = True
        relation.unchecked_count = 0
        relation.save()
   
    #self.notify_users_about_channel_change()
    def get_member_status(self, member):
        return self.members_relations.get(member=member)
    
    def is_authorized(self, sender):
        """
        Determines whether a user is authorized to send a message.
        """
        try:
            self.members.get(pk=sender.pk)
        except ObjectDoesNotExist:
            return False
        return True

    def is_admin(self, sender):
        try:
            relation = self.members_relations.get(member=sender.pk)
        except ObjectDoesNotExist:
            return False
        return relation.is_admin

    def unchecked_messages(self, member):
        """
        Returns the unchecked messages by a member
        """
        relation = self.members_relations.get(member=member)
        count = relation.unchecked_count
        return self.messages.order_by('-sent')[:count]

    def send(self, message, member=None, **kwargs):
        if member:
            message.sender = member
        if message.pk is not None:
            raise ValidationError('Message already existant. '
                                  + 'Cannot change a message\'s thread.')
        message.thread = self
        print "Send a message"
        # Send the message if the sender is authorized
        if self.is_authorized(message.sender):
            message.save()
            if 'attachment' in kwargs and kwargs.get('attachment') is not None:
                message.attachment = kwargs.get('attachment')
                message.attachment.save()
            # Message sent. Time to notify people about the new status of
            # the channel and the users
            print "Send channel change notif about : {}" \
                .format(messaging_channels.form_channel_full_name())
            self.notify_users_about_channel_change()
        else:
            raise UnauthorizedException(
                'Member {} does not belong the '
                + 'message thread. Thus, he cannot send the message.' % member)
        return message

    def notify_users_about_channel_change(self, event='channel_change'):
        if hasattr(self, 'channel'):
            print "Send channel status : {}" \
                .format(messaging_channels.form_channel_full_name())
            from .serializers import ChannelSerializerNormal
            data = {
                'slug': self.slug,
                'channel': ChannelSerializerNormal(self).data
            }
            publish(
                messaging_channels.form_channel_full_name(''),
                # Send to the root
                'channel_change',
                data,
                sender='server'
            )
        # TODO : refactor
        # - hasattr has nothing to do here
        if hasattr(self, 'privatediscussion'):
            members = [
                self.privatediscussion.initiator,
                self.privatediscussion.member
                ]
            self = self.privatediscussion
            for member in members:
                print "Send Private Discussion status : {}" \
                    .format(
                        privatediscussions_channels
                        .form_channel_full_name(
                            "member__{}".format(member.pk)
                        )
                    )
                from .serializers import PrivateDiscussionSerializerNormal
                serializer = PrivateDiscussionSerializerNormal(self)
                serializer.set_member(member)
                data = {
                    'slug': self.slug,
                    'discussion': serializer.data
                }
                publish(
                    privatediscussions_channels.form_channel_full_name("member__{}".format(member.pk)), # Send to the root 
                    'discussion_change',
                    data,
                    sender='server'
                )
        
    def print_thread(self):
        print "Thread {}".format(self.title)
        for message in self.messages.all():
            print message

    def add_member(self, member):
        if self.pk is None:
            raise self.__class__.DoesNotExist("Member {} can not join {} without saving the thread before.".format(member, self) )
        if ( self.members_relations.filter(thread=self, member=member).count() != 0 ):
            raise self.__class__.ExistingMember("Cannot add a member two times to thread {}.".format(self) )
        self.members_relations.create(thread=self, member=member, checked=True, unchecked_count=0)
    def __str__(self):
        return "Messages Thread '{}'".format(self.title) # with %i members." % (self.title, self.members.count())
    @property
    def members_count(self):
        return self.members.count()
    def save(self, *args, **kwargs):
        #if not self.slug:
        self.slug = self.form_slug()
        super(Thread, self).save(*args, **kwargs)   
    def form_slug(self):
        if self.title == '':
            raise Exception("Thread %s should contain a title in order to form a slug." % (self,) )
        slug = slugify(self.title)
        while self.__class__.objects.filter(slug=slug).exists():
            import random
            nb = random.randint(1, 100000)
            slug = "{}-{}".format(slug, nb)
        return slug

    objects = ThreadManager()
    title = models.CharField(max_length=100)
    members = models.ManyToManyField(Member, through="MemberThread")
    slug = models.SlugField(max_length=100, null=True)
    ExistingMember = ExistingMemberException
    #created = models.DateTimeField(default=datetime.now)


class ChannelManager(ThreadManager):
    
    def member_channels(self, member, relation_order=()):# relation_order=('-members_relations__unchecked_count',) ):
        #eturn self.filter(members_relations__member=member) #.order_by("-members_relations__unchecked_count")
        return self.member_threads(member, relation_order=relation_order)


class Channel(Thread):
    public = models.BooleanField(default=True)
    created = models.DateTimeField(default=datetime.now)

    objects = ChannelManager()

    def join(self, member):
        r = self.add_member(member)
        self.send(Message(type=Message.LOG, content='joined'), member=member)
        return r

    def leave(self, member):
        self.send(Message(type=Message.LOG, content='left'), member=member)
        MemberThread.objects.get(thread=self, member=member).delete()
        # self.members.remove(member)

    def send(self, message, **kwargs):
        message = super(Channel, self).send(message, **kwargs)
        from messaging.channels import channels as messaging_channels
        from messaging.serializers import MessageSerializer

        logger.debug(
            "Publish message %r to websocket channel %s"
            % (message, messaging_channels.form_thread_channel_name(self))
            )

        # Publish the message to the websocket channel
        data = MessageSerializer(message).data
        publish(
            messaging_channels.form_thread_channel_name(self),
            'message',
            data,
            sender='server'
        )
        return message


class PrivateDiscussionManager(ThreadManager):
    def discussions(
        self,
        member,
        relation_order=('members_relations__unchecked_count',)
    ):
        return self.member_threads(
            member,
            relation_order=relation_order
            ).distinct()
        return MemberThread.objects.filter(
            thread__privatediscussion__isnull=False
            ).order_by(*relation_order) \
            .prefetch_related("thread__privatediscussion").distinct()

        return self.member_threads(member, relation_order=relation_order) \
            .filter(thread__discussion__isnull=False) \
            .select_related("thread__privatediscussion") \
            .all()

    def between(self, member1, member2):
        """
        Retrieves or create a discussion between two members
        """
        try:
            discussion = self.find_discussion(member1, member2)
        except self.model.DoesNotExist:
            discussion = self.create(initiator=member1, member=member2)
        return discussion
    
    def find_discussion(self, member1, member2):
        return self.get(
            Q(
                initiator=member1,
                member=member2)
            or
            Q(
                member=member2,
                initiator=member1)
            ).distinct()

    def exists(self, member1, member2):
        return self.filter(
            Q(
                initiator=member1,
                member=member2)
            or Q(
                member=member2,
                initiator=member1)
            ).count() != 0


class ImmutableMembersList(Exception):
    pass


class PrivateDiscussion(Thread):
    initiator = models.ForeignKey(
        Member,
        related_name='discussions_initiated',
        null=True)
    member = models.ForeignKey(Member, related_name='discussions', null=True)
    objects = PrivateDiscussionManager()

    ImmutableMembersList = ImmutableMembersList

    def form_slug(self):
        slug = 'discussion-'
        if self.initiator:
            slug = slug + self.initiator.get_short_name()
        if self.member:
            slug = slug + "-" + self.member.get_short_name()
        while self.__class__.objects.filter(slug=slug).exists():
            import random
            nb = random.randint(1, 100000)
            slug = "{}-{}".format(slug, nb)

        return slug

    def clean(self):
        super(PrivateDiscussion, self).clean()
        self.title = 'Private discussion between %s and %s' \
            % (self.initiator, self.member)
        if self.objects.discussion_exists(self.initiator, self.member):
            raise ValidationError(
                'A private discussion between both members '
                + 'already exists. No nead to create a new one, please find.')
    def save(self, **kwargs):
        new = self.pk == None
        if not new:
            orig = self.__class__.objects.get(pk=self.pk)
            if self.initiator != orig.initiator or self.member != orig.member:
                raise ImmutableMembersList("Private Discussions '%s' error : Cannot change initiator or member.")
        super(PrivateDiscussion, self).save(**kwargs)
        if new:
            self.members_relations.create(member=self.initiator, thread=self, checked=True, unchecked_count=0)
            self.notify_joined(self.initiator)
            if self.initiator.pk is not self.member.pk:
                self.members_relations.create(member=self.member, thread=self, checked=True, unchecked_count=0)
            self.notify_joined(self.member)

    def notify_joined(self, member):
        self.send(Message(type=Message.LOG, content='joined'), member=member)

    def send(self, message, **kwargs):
        message = super(PrivateDiscussion, self).send(message, **kwargs)
        from messaging.serializers import MessageSerializer
        data = MessageSerializer(message).data
        #raise Exception("Focka you")
        # Publish the message to the websocket channel
        publish(
            privatediscussions_channels.form_thread_channel_name(self),
            'message',
            data,
            sender='server'
        )
        return message

class MemberThread(models.Model):
    member = models.ForeignKey(Member, related_name='threads_relations')
    thread = models.ForeignKey('Thread', related_name="members_relations")
    checked = models.BooleanField(default=False)
    unchecked_count = models.IntegerField(default=0)
    tuned = models.BooleanField(default=False)
    #tuned_since = models.DateTimeField(null=True)
    is_admin = models.BooleanField(default=False)
    date_tuned = models.DateTimeField(default=datetime.now, null=True)  

    def get_origin_thread(self, thread):
        if not isinstance(thread, Thread):
            if hasattr(thread, 'thread'):
                return thread.thread
        return thread
    
    def tune(self):
        self.tuned = True
        self.member
        self.date_tuned = datetime.now()
        
        #for thread_member_status in self.__class__.objects.filter(thread=self.thread).exclude(pk=self.pk):
        #    thread_member_status.untune()
        #self.tuned_since
        self.save()

    def untune(self):
        if self.tuned:
            self.tuned = False
            #self.thread.threads_relations.exclude(pk=self.pk).update(tuned=False)
            self.save()
    
    def is_tuned(self):
        delta = timezone.now() - self.date_tuned
        if self.tuned and (delta.seconds/60) > 2:
            self.untune()
        return self.tuned


class Message(models.Model):
    content = models.TextField()
    sender = models.ForeignKey(Member, related_name='sent_messages')
    # verb = models.CharField()
    thread = models.ForeignKey(Thread, related_name='messages')
    sent = models.DateTimeField(default=datetime.now)

    MESSAGE = 'message'
    LOG = 'log'
    INTEGRATION = 'integration'

    TYPES = (
        (MESSAGE, 'Message'),
        (LOG, 'Information'),
        (INTEGRATION, 'Integration')
    )
    type = models.CharField(max_length=20, choices=TYPES, default=MESSAGE)

    def __str__(self):
        return "<%s> said : %s" % (self.sender, self.content)


class MessageReference(models.Model):
    message = models.ForeignKey(Message, related_name='references')
    content_type = models.ForeignKey(
        ContentType,
        verbose_name='message reference content type',
        related_name='references')
    object_id = models.PositiveIntegerField('about id')
    reference = GenericForeignKey('content_type', 'object_id')


@receiver(post_save, sender=Message)
def update_members_statuses(sender, instance, **kwargs):
    print "Post Save my friend"
    thread = instance.thread
    for status in thread.members_relations.all():
        if status.member.pk is not instance.sender.pk:
            print "{} is not the sender".format(status.member)
            if not status.is_tuned():
                status.unchecked_count = status.unchecked_count + 1
                status.checked = False
                status.save()
        else:
            print "{} is the sender".format(status.member)

    #status = thread.get_member_status(sender)
    #status.checked = True
    #status.unchecked_count = 0
    
class MessageAttachment(Attachment):
    user_folder = 'msg_uploads'
    message = models.OneToOneField(Message, related_name='attachment')

    def get_author(self):
        return self.message.sender
# Entity attached to a message (Like when people are talking about a contract, a client, a lead, etc.) inside a message
#class MessageEntity(models.Model):
#   pass

"""
class MemberMessageState(models.Model):
    message = models.OneToOneField(Message)
    member = models.ForeignKey(Member)
    checked = models.BooleanField(default=False)
    archived = models.BooleanField(default=False)
"""

