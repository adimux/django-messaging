from omnibus.factories import websocket_connection_factory, sockjs_connection_factory
from messaging.models import *
from django.db import connection
from install.models import AgfUser

def zpark_connection_factory(auth_class, pubsub):
	# Generate a new connection class using the default websocket connection
	# factory (we have to pass an auth class - provided by the server and a
	# pubsub singleton, also provided by the omnibusd server
	class GeneratedConnection(sockjs_connection_factory(auth_class, pubsub)):
		def on_message(self, msg):
			super(GeneratedConnection, self).on_message(msg)
		"""
		@return {boolean} publish : publish the message
		"""
		def call_handler(self, channel, payload, **kwargs):
			#parts = channel.split(".")

			schema_name =''
			module = ''
			handler = ''
			subchannel = ''

			comma_index = channel.index(",")
		
			first = channel[0:comma_index]
			last = channel[comma_index+1:]
			
			print "First"
			print first
			print "Last"
			print last

			parts = first.split(".")
			
			if len(parts) < 2:
				return True
			else:
				schema_name = parts[0]
				module = ".".join(parts[1:])

			parts = last.split(".")
	
			print "Parts"
			print parts
			
			if len(parts) is not 2:
				return True
			else:
				handler = parts[0]
				subchannel = parts[1]

			print "Parts"
			print parts
			
			connection.schema_name = schema_name
			twodots_index = payload.index(":")
			
			import simplejson
			data = simplejson.loads( payload[twodots_index+1:] )
			from importlib import import_module
			mod = import_module(module + ".channels")

			handler_class = getattr(mod, handler)
	
			#handler_class = getattr(import_module(module), handler)
			print "Handler"
			print handler_class
			handler_instance = handler_class(self)
			
			action = handler_instance.handle_channel_message(subchannel, data)

			return action

		def on_channel_message(self, channel, payload):
			if (
				channel in self.subscriber.channels
				and self.authenticator.can_publish(channel)
			):
				print "channel"
				print channel
			
				publish_it = self.call_handler(channel, payload)
				
				print "Republish it ??"
				print publish_it
				
				if publish_it == True:
					self.publish(payload)
				#elif isinstance(publish_it, dict):
				#	self.publish(payload)
		#def on_command_message(self, channel, payload):
			
		"""
		def close_connection(self):
			# We subclassed the `close_connection` method to publish a
			# message. Afterwards, we call the parent's method.
			self.pubsub.publish(
				'mousemoves', 'disconnect',
				sender=self.authenticator.get_identifier()
			)
			return super(GeneratedConnection, self).close_connection()
		"""

	# Return the generated connection class
	return GeneratedConnection


# Our factory function
def mousemove_connection_factory(auth_class, pubsub):
	# Generate a new connection class using the default websocket connection
	# factory (we have to pass an auth class - provided by the server and a
	# pubsub singleton, also provided by the omnibusd server
	class GeneratedConnection(websocket_connection_factory(auth_class, pubsub)):
		def close_connection(self):
			# We subclassed the `close_connection` method to publish a
			# message. Afterwards, we call the parent's method.
			self.pubsub.publish(
				'mousemoves', 'disconnect',
				sender=self.authenticator.get_identifier()
			)
			return super(GeneratedConnection, self).close_connection()

	# Return the generated connection class
	return GeneratedConnection
