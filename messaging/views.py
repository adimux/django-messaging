# -*- coding: utf-8 -*-
import json
from datetime import datetime
import dateutil.parser
from datetime import timedelta
from decimal import *
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, mixins
from django.http import HttpResponse
from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.decorators import *
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from django.contrib.auth.decorators import login_required
from django.db import models
from .models import *
from messaging.models \
    import (
        get_member_from_request,
        Member,
        Message,
        Thread
        )
from .serializers import *
from messaging.serializers \
    import (
        MessageSerializer,
        )
from rest_framework.authentication \
    import (
        SessionAuthentication,
        BasicAuthentication,
        TokenAuthentication
        )
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import serializers
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import filters
from rest_framework.pagination import PageNumberPagination, LimitOffsetPagination, CursorPagination
from core.views import StandardLimitOffsetPagination
from rest_framework_extensions.mixins import NestedViewSetMixin
from django.core.exceptions import ObjectDoesNotExist
from rest_framework_extensions.decorators import action, link
from rest_framework import filters
from rest_framework import generics
from rest_framework.decorators import detail_route, list_route
import django_filters
from django.db.models import Q
from rest_framework import permissions
from rest_framework import serializers
from core.models import AgfUser
from core.utils import get_url, get_model
from django.core.exceptions import ValidationError
from rest_framework import status
import logging

logger = logging.getLogger(__name__)


class IsThreadAdminPermission(permissions.BasePermission):
    """
    Permission Check for editing a channel
    """
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        if request.method == 'PATCH':
            if isinstance(obj, MemberThread):
                return obj.thread.is_admin(member)

    def has_permission(self, request, view):
        if request.method == 'POST':
            thread = view.thread
            try:
                member = Member.objects.get(pk=request.user.pk)
            except ObjectDoesNotExist:
                return False
            return thread.is_admin(member)
        elif request.method == 'GET':
            return True

class MessageCursorPagination(CursorPagination):
    page_size = 20
    ordering = '-sent'

class MessageLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 30
    max_limit = 40


class MemberThreadViewSet(
    NestedViewSetMixin,
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet
        ):
    queryset = MemberThread.objects.all()
    serializer_class = MemberThreadSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    permission_classes = (permissions.IsAuthenticated, IsThreadAdminPermission)

    def initial(self, request, *args, **kwargs):
        self.thread = self.get_message_thread(request, **kwargs)
        super(MemberThreadViewSet, self).initial(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(thread=self.thread)

    def get_message_thread(self, request, **kwargs):
        if 'parent_lookup_thread' in kwargs:
            thread = Thread.objects.get(pk=int(kwargs['parent_lookup_thread']))
            if hasattr(thread, 'channel'):
                return thread.channel
            if hasattr(thread, 'privatediscussion'):
                return thread.privatediscussion
            return thread
        else:
            return None

import django_filters

class MessageFilter(filters.FilterSet):
    class Meta:
        model = Message
        fields = {'sender':{}, 'content':{}, 'sent':{}}
        #'attachment__file':{}, 
        

        filter_overrides = {
            models.FileField: {
                'filter_class': django_filters.CharFilter,
            }
        }   
#    attachment = django_filters.CharFilter(method='attachment_filter')

#    def attachment_filter(self, queryset, search):
#        return queryset.filter(attachment__file__search=search)

class MessageReferenceViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = MessageReference.objects.all()
    serializer_class = MessageReferenceSerializer
    authentication_classes = (
        SessionAuthentication,
        BasicAuthentication,
        TokenAuthentication
        )


class MessageViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    #pagination_class = MessageCursorPagination
    #pagination_class = StandardLimitOffsetPagination
    pagination_class = MessageLimitOffsetPagination
    filter_class = MessageFilter
    filter_backends = (filters.SearchFilter,filters.DjangoFilterBackend,filters.OrderingFilter)
    search_fields = ('content', 'attachment__file')
    
    ordering_fields = ('sent',)
    ordering=('-sent',)
    
    def perform_create(self, serializer):
        serializer.save(thread=self.thread)
        #thread = self.thread
        #sender = Member.objects.get(pk=self.request.user.pk)
        #sent_message = thread.send(Message(**serializer.data), sender)
        #message_data = MessageSerializer(sent_message).data
        
        #return Response(message_data)

        #message = serializer.data
        """
        if self.thread.channel:
            from messaging.channels import channels as messaging_channels
            data = serializer.data
            publish(
                messaging_channels.form_channel_full_name(message.thread.channel.slug),
                'message',
                data,
                sender='server'
            )
        """
    def create(self, request, **kwargs):
        thread = self.get_message_thread(request, **kwargs)

        logger.debug("Preparing to create a message in thread %r" % thread)

        sender = get_member_from_request(request)
        context = {
            'request': request,
            'member': sender
        }
        message_serializer = MessageSerializer(
            data=request.data,
            context=context
            )
        message_serializer.is_valid(raise_exception=True)
        message_data = dict((key, value) for key, value in
                            message_serializer.data.iteritems()
                            if key in ('content', ))

        sent_message = thread.send(Message(**message_data), member=sender)
        serialized_message = MessageSerializer(
            sent_message,
            context=context
            ).data

        return Response(serialized_message)
        # return super(MessageViewSet, self).create(request, **kwargs)

    def get_message_thread(self, request, **kwargs):
        if 'parent_lookup_thread' in kwargs:
            thread = Thread.objects.get(pk=int(kwargs['parent_lookup_thread']))
            if hasattr(thread, 'channel'):
                return thread.channel
            if hasattr(thread, 'privatediscussion'):
                return thread.privatediscussion
            return thread
        else:
            return None


#class ChannelMessageViewSet(MessageViewSet):

#@list_route(methods=['GET'])
#def unread(self, request, thread_pk=None):
#   queryset = self.get_queryset().unchecked_messages(request.user)
#   page = self.paginate_queryset(queryset)
#   if page is not None:
#       return self.get_paginated_response(self.get_serializer_class()(page, many=True, context={'request':request}).data)

#   return Response(self.get_serializer_class()(queryset, many=True, context={'request':request}).data)
    
#def see(self, request, thread_pk=None):
def get_member_from_view(view):
    request = view.request
    member = get_member_from_request(request)
    return member


from rest_framework.response import Response
class ThreadViewSetMixin(object):
    def get_thread(self, request, pk=None):
        return self.get_queryset().get(pk=pk)
    @action()
    def see(self, request, pk=None):
        channel = self.get_thread(request, pk=pk)
        channel.see( get_member_from_view(self) )
        return Response( self.get_serializer(channel, context={'request':request} ).data )
        return Response({'status':'ok'})
    @action()
    def tune(self, request, pk=None):
        channel = self.get_thread(request, pk=pk)
        channel.tune(get_member_from_view(self))
        return Response(self.get_serializer(channel, context={'request': request}).data)
    @action()
    def untune(self, request, pk=None):
        channel = self.get_thread(request, pk=pk)
        channel.untune(get_member_from_view(self))
        return Response(
            self.get_serializer(
                channel,
                context={
                    'request': request
                }).data)

    """
    TODO : Use the same technique as used in MessageViewSet.create() function
    @action()
    def send(self, request, pk=None):
        thread = self.get_queryset().get(pk=pk)
        sender = Member.objects.get(pk=request.user.pk)
        sent_message = thread.send(Message(**message_dict), sender)
        message_data = MessageSerializer(
            sent_message,
            context={'request': request}
            ).data
        return Response(message_data)
    """

class ChannelViewSet(viewsets.ModelViewSet, ThreadViewSetMixin):
    serializer_class = ChannelSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
        
    filter_backends = (filters.SearchFilter,filters.DjangoFilterBackend,filters.OrderingFilter)
    search_fields = ('title',)
    #filter_class = ChannelFilter
    #ordering_fields = ('id',)
    
    pagination_class = StandardLimitOffsetPagination

    def get_queryset(self):
        member = get_member_from_view(self)
        return Channel.objects.member_channels(member)
    @action()
    def leave(self, request, pk=None):
        channel = self.get_thread(request, pk=pk)
        channel.leave( get_member_from_view(pk=request.user.pk) )
        return Response( ChannelSerializer(channel, context={'request':request} ).data )
    @action()
    def invite(self, request, pk=None):
        channel = self.get_thread(request, pk=pk)
        if 'members' in request.data:
            if not isinstance(request.data['members'], list):
                raise ValidationError(
                    {
                        'members': ['Must be an array']
                    })
            for member_url in request.data['members']:
                #member_url = request.data.get('member')
                member = get_model(
                    member_url,
                    'messaging:member-detail',
                    model=Member,
                    context={
                        'request': request
                    })
                try:
                    channel.join(member)
                except ExistingMemberException:
                    return Response({
                            'member': ['Member {} is already member of the channel {}'.format(member, channel)]
                        },
                        status=status.HTTP_400_BAD_REQUEST ) 
                #return Response( ChannelSerializer(channel, context={'request':request} ).data )

            return Response(
                {
                    'members': ['Members added']
                })
        elif 'users' in request.data:
            for user in request.data['users']:
                user = get_model(
                    user,
                    'agfuser-detail',
                    model=AgfUser,
                    context={'request':request}
                    )
                member = Member.objects.get(pk=user.pk)
                channel.join(member)

                return Response(
                    ChannelSerializer(
                        channel,
                        context={
                            'request': request
                        }).data)

            return Response({
                'members': ['Member added']
                })
        else:
            raise ValidationError({
                'non_field_errors':
                    ['Either user url or member url need to be present in the request data.']
                })


class PrivateDiscussionViewSet(viewsets.ModelViewSet, ThreadViewSetMixin):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)

    def filter_queryset(self, queryset):
        member = get_member_from_view( self )
        return PrivateDiscussion.objects.discussions(member)
    
    serializer_class = PrivateDiscussionSerializer
    queryset = PrivateDiscussion.objects.all()
    pagination_class = StandardLimitOffsetPagination


    @list_route(methods=['GET'])
    def get_with(self, request):
        member1 = get_member_from_view(self)
        raise Exception(request.query_params)
        if 'member' in request.query_params:
            member_pk = request.query_params['member']
            member2 = Member.objects.get(pk=member_pk)

            raise Exception('problem')

            discussion = PrivateDiscussion.objects.between(member1, member2)

            return Response(PrivateDiscussionSerializer(discussion).data)
        return Response({})


class MemberFilter(filters.FilterSet):
    class Meta:
        model = Member
        fields = ('thread',)
        # fields = {'threads_relations__thread':['exact', 'not_exact'],}
    # belongs_to = filters.MethodFilter
    # doesnt_belongs_to = filters.MethodFilter
    thread = django_filters.ModelChoiceFilter(
        queryset=Thread.objects.all(),
        name='threads_relations__thread')
    not_in_thread = django_filters.ModelChoiceFilter(
        queryset=Thread.objects.all(),
        name='threads_relations__thread',
        exclude=True,
        distinct=True)


class MemberViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)

    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter)
    search_fields = ('username', 'first_name', 'last_name', 'email', )
    filter_class = MemberFilter
    ordering_fields = ()
    
    serializer_class = MemberSerializer
    queryset = Member.objects.all()


class MemberHkViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)

    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend, filters.OrderingFilter)
    search_fields = ('username', 'first_name', 'last_name', 'email', )
    filter_class = MemberFilter
    ordering_fields = ()

    serializer_class = MemberHkSerializer
    queryset = Member.objects.all()
