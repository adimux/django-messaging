# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import core.models
import django.contrib.auth.models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MemberThread',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('checked', models.BooleanField(default=False)),
                ('unchecked_count', models.IntegerField(default=0)),
                ('tuned', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField()),
                ('sent', models.DateTimeField(default=datetime.datetime.now)),
                ('type', models.CharField(default='message', max_length=20, choices=[('message', 'Message'), ('log', 'Information'), ('integration', 'Integration')])),
            ],
        ),
        migrations.CreateModel(
            name='MessageAttachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(default=b'', max_length=255)),
                ('file', models.FileField(upload_to=core.models.user_directory_uploads_path)),
                ('message', models.ForeignKey(related_name='attachments', to='messaging.Message')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Thread',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Member',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('core.agfuser',),
            managers=[
                (b'objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Channel',
            fields=[
                ('thread_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='messaging.Thread')),
                ('public', models.BooleanField(default=True)),
                ('created', models.DateTimeField(default=datetime.datetime.now)),
            ],
            bases=('messaging.thread',),
        ),
        migrations.CreateModel(
            name='PrivateDiscussion',
            fields=[
                ('thread_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='messaging.Thread')),
                ('initiator', models.ForeignKey(related_name='discussions_initiated', to='messaging.Member', null=True)),
                ('member', models.ForeignKey(related_name='discussions', to='messaging.Member', null=True)),
            ],
            bases=('messaging.thread',),
        ),
        migrations.AddField(
            model_name='thread',
            name='members',
            field=models.ManyToManyField(to='messaging.Member', through='messaging.MemberThread'),
        ),
        migrations.AddField(
            model_name='message',
            name='sender',
            field=models.ForeignKey(related_name='sent_messages', to='messaging.Member'),
        ),
        migrations.AddField(
            model_name='message',
            name='thread',
            field=models.ForeignKey(related_name='messages', to='messaging.Thread'),
        ),
        migrations.AddField(
            model_name='memberthread',
            name='member',
            field=models.ForeignKey(related_name='threads_relations', to='messaging.Member'),
        ),
        migrations.AddField(
            model_name='memberthread',
            name='thread',
            field=models.ForeignKey(related_name='members_relations', to='messaging.Thread'),
        ),
    ]
