# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('messaging', '0007_auto_20160903_1942'),
    ]

    operations = [
        migrations.CreateModel(
            name='MessageReference',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.PositiveIntegerField(verbose_name='about id')),
                ('content_type', models.ForeignKey(related_name='references', verbose_name='message reference content type', to='contenttypes.ContentType')),
                ('message', models.ForeignKey(related_name='references', to='messaging.Message')),
            ],
        ),
    ]
