================
Django Messaging
================

Django Messaging is a simple Django app that implements the REST-API of a chat system, as well as the WebSocket communication of it.

Users can engage in discussions between each other or create groups of discussions and participate in them.

In addition, an instant messaging integration is available where the messages are handled through WebSockets with the help of omnibus.

Quick Start
-----------

1. Install 'omnibus', 'django-omnibus' and 'django-messaging'

    pip install omnibus django-omnibus django-messaging

2. Add 'omnibus' and 'django-messaging' to your INSTALLED_APPS like this.

    INSTALLED_APPS = [
        ...,
        'omnibus',
        'django-messaging'
    ]

3. Include the messaging REST-API URLconf in your project urls.py:

    url(r'^messaging/', 
        include('messaging.urls', namespace='messaging')),

4. Run the migration to create the messaging models.

    python manage.py migrate

5. Start the development server and visit http://localhost:8000/messaging/ to explore the REST-API.

Instant messaging System
------------------------

1. If you want to use the instant messaging system, configure omnibus this way:

    OMNIBUS_ENDPOINT_SCHEME = 'http'  # 'ws' is used for websocket connections
    OMNIBUS_WEBAPP_FACTORY = 'omnibus.factories.sockjs_webapp_factory' 
    OMNIBUS_CONNECTION_FACTORY = 'messaging.connection.zpark_connection_factory'
    OMNIBUS_AUTHENTICATOR_FACTORY = 'messaging.factories.usertokenauthenticator_factory'

2. Here is the command to launch omnibus :

    python manage.py omnibusd
