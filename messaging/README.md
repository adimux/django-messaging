## Settings

Add 'omnibus' to your APPS in the settings file.

Add to your settings :

"""
OMNIBUS_ENDPOINT_SCHEME = 'http'  # 'ws' is used for websocket connections
OMNIBUS_WEBAPP_FACTORY = 'omnibus.factories.sockjs_webapp_factory'
# OMNIBUS_CONNECTION_FACTORY = 'omnibus.factories.sockjs_connection_factory'
OMNIBUS_CONNECTION_FACTORY = 'messaging.connection.zpark_connection_factory'
# OMNIBUS_AUTHENTICATOR_FACTORY = 'omnibus.factories.userauthenticator_factory'
OMNIBUS_AUTHENTICATOR_FACTORY = 'messaging.factories.usertokenauthenticator_factory'
# OMNIBUS_AUTHENTICATOR_FACTORY = 'omnibus.factories.noopauthenticator_factory'
"""
