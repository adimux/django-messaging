# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('messaging', '0005_memberthread_date_tuned'),
    ]

    operations = [
        migrations.AlterField(
            model_name='memberthread',
            name='date_tuned',
            field=models.DateField(default=datetime.datetime.now, null=True),
        ),
    ]
