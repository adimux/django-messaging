# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('messaging', '0003_memberthread_is_admin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='messageattachment',
            name='message',
            field=models.OneToOneField(related_name='attachment', to='messaging.Message'),
        ),
    ]
