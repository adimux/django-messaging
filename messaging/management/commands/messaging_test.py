from contrat.management.commands.contrat_tests import SchemaSet
from contrat.models import *
from contrat.views import *
from agf.models import *
from agf.models import Client as AGFClient
from datetime import datetime, timedelta
from install.serializers import *
from install.models import *
from messaging.models import *
from django.core.exceptions import ObjectDoesNotExist

class Command(SchemaSet):
	def do_tests(self):
		self.destroy_all = True
		self.test_channel()
		self.test_private_discussion()
	def add_arguments(self, parser):
		parser.add_argument(
			'--keepalive',
			action='store_true',
			dest='keepalive',
			#const='zpark.ca',
			help="Keep created models alive in order to use them in the future."
		)
	def initialize(self):
		super(Command, self).initialize()

		# Whether to leave the created models alive
		self.keepalive = self.options.get('keepalive', False)

		# Create a messages' receiver
		self.receiver, cr = AgfUser.objects.get_or_create(username='receiver', email='receiver@zpark.ca', first_name="Receiver")
		self.receiver = Member.objects.get(pk=self.receiver.pk)
		# Create another user 
		self.other_user,cr = AgfUser.objects.get_or_create(username='stranger', email='stranger@zpark.ca', first_name="Strangeoo")
		self.other_user = Member.objects.get(pk=self.other_user.pk)
		# Create a messages' sender
		self.sender = self.user
		self.sender = Member.objects.get(pk=self.sender.pk)
		# Create a lost user who sends messages without being invited to the party :)
		self.lost_user,cr = AgfUser.objects.get_or_create(username='lost', email='TDAH@zpark.ca', first_name='Adam', last_name='Cherti')
		self.lost_user = Member.objects.get(pk=self.lost_user.pk)
	def test_channel(self):
		# Create channel
		channel = self.create_channel()
		try:
			# And send a message by 'other user'
			self.send_message(self.other_user, channel)
			raise AssertionError("Normally, a user not belonging to a channel should not be capable of sending messages to the channel")
		except UnauthorizedException:
			pass
			# Good
		self.send_message(self.sender, channel)
		if not self.keepalive:
			channel.delete()
	def test_private_discussion(self):
		self.create_private_discussion()
	def same_permutation(self, a, b):
		if len(a) != len(b):
			return False
		for x in a:
			exists = False
			for y in b:
				if x == y:
					exists = True

			if not exists:
				return False
		return True

	def create_channel(self):
		"""
		Create a channel "Zpark Enginering" with the receiver and sender as members
		"""
		channel = Channel.objects.create(title='Zpark Engineering')
		channel.join(self.receiver)
		channel.join(self.sender)
		#channel.join(self.other_user)
		members = channel.members.all()
		# Test whether the members are the right ones
		correct_members = [self.receiver, self.sender]
		assert( self.same_permutation(members, correct_members) == True),"Channel Creation Error : Wrong members."
		return channel
	def send_message(self, user, channel):
		"""
		Sends a message to a channel, sent by the user given as argument.
		Tests include (about the last sent message) :
		- The assertion that the last sent message is marked as having been sent by this same user
		- The assertion that the last message has been sent to the right thread, 
		- The assertion that the last message's type is MESSAGE
		- Assertion that the content is the one we provided when we created it
		- Assertion that the unchecked_count variable has increased and  that the 'checked' variable
					has been set to False for every user (in its relation to the thread) except for the sender of the message.
		
				Finally, delete the message
		"""
		message_content = 'Salut tout le monde :))'
		message = Message(content=message_content)
		# Try to send a message by a non-member of the channel
		try:
			channel.send(message, self.lost_user)
			raise AssertionError("Channel (%s) accepted a message to be sent by a non-member user : %s !"% (self.lost_user))
		except UnauthorizedException as e:
			pass # Test passed
		
		# Save members relations
		relations = channel.members_relations.all()
		
		relations_dicts = []
		logger = logging.getLogger(__name__)
		
		for relation in relations:
			if relation.member != user:
				relation.checked = False
				relation.unchecked_count = relation.unchecked_count + 1
			print "Member <> Thread Status"
			print relation.__dict__
			
			relations_dicts.append( relation.__dict__ )

		# User sends the message in the channel 
		channel.send(message, user)
		
		# Get latest message and assert some values
		latest_message = channel.messages.latest('id')
		
		assert (latest_message.content == message_content), 'Sending message to channel error : Not the same content.'
		assert (latest_message.sender == user), 'Sending message to channel error : Not the right sender.'
		assert (latest_message.thread == channel), 'Sending message to channel error : It has been sent to the wrong channel.'
		assert (latest_message.type == Message.MESSAGE), ''
		
		relations = channel.members_relations.all()
		user_thread_status = relations.get(member=user)
		
		assert (user_thread_status.checked == True and user_thread_status.unchecked_count == 0), "Member {} status related to the thread should be checked == True, uncheckec_count == 0. Got {}".format(user, user_thread_status.__dict__)

		for relation_dict in relations_dicts:
			rel = relations.get(member=relation_dict['_member_cache'].pk)
			if relation_dict['_member_cache'].pk is not user.pk:
				member = relation_dict['_member_cache']
				print "Lets look closely at him"	
				print member
				channel.print_thread()
				print channel.get_member_status(member).__dict__
				print channel.unchecked_messages( member )
				assert( channel.unchecked_messages( member )[0] == latest_message ), "Last unchecked message for user {} is not the right one. Expected message : {} , got {}.".format(member, latest_message, channel.unchecked_messages( member )[0] )

			for key in ["checked", "unchecked_count"]:
				val = relation_dict[key]
				assert ( getattr(rel, key) == relation_dict[key] ), "Thread User Status different than expected. Expecting {} to be '{}'. Actual value : {}.".format(key, val, getattr(rel,key) )
			# Mark the thread as seen by the member
			channel.see(rel.member)
			rel = relations.get(member=relation_dict['_member_cache'].pk)
			
			relation_dict['unchecked_count'] = 0
			relation_dict['checked'] = True
			# check again if the correct values has been affected
			for key in ["checked", "unchecked_count"]:
				print relation_dict
				assert ( getattr(rel, key) == relation_dict[key] ), "Thread User Status different than expected after the member notified that he checked the conversation. Expecting %s to be %s. Actual value : %s." % (key, val, getattr(rel,key) )
		

		if not self.keepalive:
			message.delete()
	def create_private_discussion(self):
		discussion = None
		message = None
		try:
			"""
			Create a private discussion between two people, one being tagged as an initiator and the oser as e simple member.
			"""
			discussion = PrivateDiscussion(initiator=self.sender, member=self.receiver)
			discussion.save()

			# Ensure that the last discussion created for every member is equal to 'discussion' 
			assert( PrivateDiscussion.objects.discussions(self.sender).latest("id").pk == PrivateDiscussion.objects.discussions(self.receiver).latest("id").pk ), "The last discussion created for every member must be equal to the discussion that we just created."

			# Try to add a member to the discussion and 
			assert( self.same_permutation( [self.receiver, self.sender], discussion.members.all() ) ), "Members of the private discussion are not the ones expected. Expected %s , got %s." % ([self.receiver, self.sender], discussion.members.all() )
			message_content = "Hey beautiful donkey.. :)"
			message = Message(content=message_content)
		
			# Try to send a message by a non-member of the channel
			try:
				discussion.send(message, self.lost_user)
				raise AssertionError("Channel (%s) accepted a message to be sent by a non-member user : %s !"% (self.lost_user))
			except UnauthorizedException as e:
				pass # Test passed
		
			sender_relation_dict_expected = discussion.get_member_status(self.sender).__dict__
			receiver_relation_dict_expected = discussion.get_member_status(self.receiver).__dict__
		
			# That's what it should be if the sender sends a message
			#sender_relation_dict_expected['checked'] = True # Should stay the same
			#sender_relation_dict_expected['unchecked_count'] = 0 # Should stay the same
			receiver_relation_dict_expected['checked'] = False
			receiver_relation_dict_expected['unchecked_count'] = receiver_relation_dict_expected['unchecked_count'] + 1

			# 'sender' sends a message
			discussion.send(message, self.sender)
		
			# Assert the both receiver and sender status after sending the message are the ones expected.
			# That means that checked == False for receiver and unchecked_count has been incremented by one. While for the sender, checked == True and unchecked_count should stay the same.
			sender_relation_dict = discussion.get_member_status(self.sender).__dict__
			receiver_relation_dict = discussion.get_member_status(self.receiver).__dict__
		
			sender_unmatched_items = set(sender_relation_dict_expected) ^ set(sender_relation_dict)
			receiver_unmatched_items = set(receiver_relation_dict_expected) ^ set(receiver_relation_dict)
			assert( len(sender_unmatched_items) == 0), "Unmateched items between what was expected for the sender status in the discussions. Expected %s , got %s" % (sender_relation_dict_expected, sender_relation_dict)
			assert( len(receiver_unmatched_items) == 0), "Unmateched items between what was expected for the receiver status in the discussions. Expected %s , got %s " % (receiver_relation_dict_expected, receiver_relation_dict)
		except:
			if not self.keepalive:
				if message and message.pk:
					message.delete()
				if discussion and discussion.pk:
					discussion.delete()
			raise
	def destroy(self):
		super(Command, self).destroy()
		if not self.keepalive:
			self.receiver.delete()
			self.other_user.delete()
			self.lost_user.delete()

