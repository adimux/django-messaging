from rest_framework import serializers
from core.serializers \
    import (
        EntitySerializerMixin,
        )
# from core.serializers import EventSerializer as TheEventSerializer
from .models import (
    Message,
    Channel,
    PrivateDiscussion,
    get_member_from_user,
    MessageAttachment,
    Member,
    MessageReference,
    MemberThread
    )
import logging
from install.serializers import MessagingMemberSerializer
from core.serializers import AttachmentSerializer
from manager.registry import models_serializers
# from generic_relations.relations import GenericRelatedField

logger = logging.getLogger(__name__)


def get_member_from_serializer(serializer):
    request = serializer.context.get('request', None)
    if 'member' in serializer.context:
        return serializer.context.get('member')
    elif request:
        return get_member_from_user(request.user)
    raise Exception("Cannot get member from serializer"
                    + " if request is not in the context")


class ThreadSerializerMixin(EntitySerializerMixin):
    member_status = serializers.SerializerMethodField()
    members_count = serializers.IntegerField(read_only=True)

    class Meta:
        fields = (
            'members_count',
            'title',
            'slug',
            'member_status',
            'thread_ptr')

    thread_ptr = serializers.IntegerField(
        read_only=True,
        source='thread_ptr.id')

    def set_member(self, member):
        self.context['member'] = member

    def get_thread(self, thread):
        return thread

    def get_member_status(self, thread):
        channel = self.get_thread(thread)
        try:
            if 'member' in self.context:
                member = self.member
            elif 'request' in self.context:
                member = get_member_from_serializer(self)

            if member:
                relation = channel.members_relations.get(member=member)

            return MemberThreadSerializer(
                relation,
                context=self.context
                ).data
        except:
            return {}


class ChannelSerializerMixin(ThreadSerializerMixin):
    class Meta:
        model = Channel
        # fields = ('members',  'public', 'created', 'thread_ptr')

        fields = ThreadSerializerMixin.Meta.fields \
            + ('members',  'public', 'created')
    """
    thread_ptr = serializers.IntegerField(
        read_only=True,
        source='thread_ptr.id')
    """


class ChannelSerializerNormal(
        ChannelSerializerMixin,
        serializers.ModelSerializer):
    class Meta:
        model = Channel
        fields = ChannelSerializerMixin.Meta.fields


class ChannelSerializer(
        ChannelSerializerMixin,
        serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Channel
        extra_kwargs = {
            'url': {'view_name': 'messaging:channel-detail'},
            'members': {'view_name': 'messaging:member-detail'},
        }
        fields = ChannelSerializerMixin.Meta.fields + ('url',)


class PrivateDiscussionSerializerMixin(ThreadSerializerMixin):
    class Meta:
        model = PrivateDiscussion
        extra_kwargs = {
            'title': {'read_only': True},
        }
        fields = (
            'members_count',
            'title',
            'slug',
            'member_status',
            'thread_ptr',
            'members',
            'initiator',
            'member')

    def create(self, data):
        return PrivateDiscussion \
            .objects \
            .between(
                get_member_from_serializer(self),
                data.get('member')
            )
        """
        if not ('initiator' in data):
            data['initiator'] = get_member_from_serializer(self)
        return super(PrivateDiscussionSerializerMixin, self).create(data)
        """


class PrivateDiscussionSerializer(
    PrivateDiscussionSerializerMixin,
        serializers.HyperlinkedModelSerializer):
    """
    class Meta(PrivateDiscussionSerializerMixin.Meta):
        model = PrivateDiscussion
        extra_kwargs = {
            'url': {'view_name': 'messaging:privatediscussion-detail'},
            'member':{'view_name':'messaging:member-detail'},
            'initiator': {
                'view_name': 'messaging:member-detail',
                'read_only':True},
            'title':{'read_only':True},
            'members':{'view_name':'messaging:member-detail'},
            'slug':{'read_only':True}
        }
        #fields = (
            'members_count',
            'title',
            'slug',
            'member_status',
            'thread_ptr',
            'members',
            'url') + ('other_member',)

        fields = PrivateDiscussionSerializerMixin.Meta.fields \
            + ('other_member',)
    """
    class Meta:
        model = PrivateDiscussion
        extra_kwargs = {
            'url': {'view_name': 'messaging:privatediscussion-detail'},
            'member': {'view_name': 'messaging:member-detail'},
            'initiator': {
                'view_name': 'messaging:member-detail',
                'read_only': True
                },
            'title': {'read_only': True},
            'members': {'view_name': 'messaging:member-detail'},
            'slug': {'read_only': True}
        }
        fields = PrivateDiscussionSerializerMixin.Meta.fields \
            + ('url', 'other_member')

    other_member = serializers.SerializerMethodField()

    def get_other_member(self, privatediscussion):
        """
        @return {member serialized} : Get the other party (member) that
            we are talking with. If we are a part of the discussion.
        """
        request_member = get_member_from_serializer(self)
        other_member = None
        if privatediscussion.belongs(request_member):
            # Compare the 'initiator' and 'member' with the request member
            # to find the party that we are talking with
            if privatediscussion.initiator.pk == request_member.pk:
                other_member = privatediscussion.member
            elif privatediscussion.member.pk == request_member.pk:
                other_member = privatediscussion.initiator
            if other_member:
                return MemberHkSerializer(
                    other_member,
                    context=self.context
                    ).data
        return None


class PrivateDiscussionSerializerNormal(
    PrivateDiscussionSerializerMixin,
        serializers.ModelSerializer):
    pass


class MemberSerializer(MessagingMemberSerializer):
    pass
    # url = serializers.HyperlinkedIdentityField(view_name='member-detail')


class MemberHkSerializer(MessagingMemberSerializer):
    class Meta(MessagingMemberSerializer.Meta):
        fields = MessagingMemberSerializer.Meta.fields + ('url', )
    url = serializers.HyperlinkedIdentityField(
        view_name="messaging:member-detail"
        )

"""
class MemberSerializer(
    MoralEntityMixin,
    serializers.HyperlinkedModelSerializer, ):
    class Meta:
        model = Member
        extra_kwargs = {'url':{'view_name':'messaging:member-detail'}}
        fields = (
            'url',
            'is_superuser',
            'username',
            'first_name',
            'last_name',
            'email',
            "is_staff",
            'is_active',)
"""


class MessageAttachmentSerializer(AttachmentSerializer):
    class Meta:
        model = MessageAttachment
        fields = (
            'type',
            'file',
            'extension',
            'mimetype',
            'name',
            'general_type')
        extra_kwargs = {
            'type': {'read_only': True}
        }


class MessageReferenceSerializer(serializers.ModelSerializer):
    reference = serializers.SerializerMethodField()
    entity = serializers.SerializerMethodField()
    pk = serializers.SerializerMethodField()
    """
    detail = serializers.SerializerMethodField()
    def get_detail(self, reference):
        from manager.registry import models_serializers
        data = None
        #raise Exception( reference.reference.__class__ )
        if reference.reference.__class__ in models_serializers:
            serializer = models_serializers[reference.reference.__class__]
            data = serializer( reference.reference, context=self.context ).data
        return data
    """
    def get_reference(self, message_reference):
        model = message_reference.reference.__class__

        if model in models_serializers:
            serializer = models_serializers[model]
            return serializer(
                message_reference.reference,
                context=self.context).data
        return None

    class Meta:
        model = MessageReference

    def get_entity(self, obj):
        ctype = obj.content_type
        entity = ctype.app_label + "." + ctype.model
        return entity

    def get_pk(self, obj):
        return obj.object_id


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        extra_kwargs = {
                # 'url': {
                #   'view_name':'messaging:message-detail'
                # },
                'thread': {
                    'read_only': True
                },
                'sent': {
                    'read_only': True
                }
            }
        fields = (
            'attachment',
            'sender',
            'content',
            'id',
            'sent',
            'type',
            'references')
    references = MessageReferenceSerializer(many=True, required=False)
    sender = MemberSerializer(read_only=True)
    attachment = MessageAttachmentSerializer(required=False)
    """
    thread = GenericRelatedField({
        Channel: serializers.HyperlinkedRelatedField(
            queryset=Channel.objects.all(),
            view_name='messaging:channel-detail',
        ),
        PrivateDiscussion: serializers.HyperlinkedRelatedField(
            queryset=PrivateDiscussion.objects.all(),
            view_name='messaging:privatediscussion-detail',
        ),
    }, read_only=True)
    """
    def validate_context(self):
        assert 'request' in self.context or 'member' in self.context, \
            "To use %s in write mode, it needs the context." \
            % (self.__class__,)

    def validate(self, attrs):
        self.validate_context()

        attrs = super(MessageSerializer, self).validate(attrs)
        attrs['sender'] = get_member_from_serializer(self)
        # attrs['thread'] = Thread.objects.get(pk=self
        return attrs

    def create(self, validated_data):
        message_attachment_data = None
        # references_data = None
        if 'attachment' in validated_data:
            message_attachment_data = validated_data.pop('attachment')
        # if 'references' in validated_data:
        #     references_data = validated_data.pop('references')
        # TODO : Make use of references_data
        # message = super(MessageSerializer, self).create(validated_data)
        message_att = None
        if message_attachment_data:
            message_att = MessageAttachment(**message_attachment_data)
            # validated_data['attachment'] = message_att
        message = Message(**validated_data)
        # raise Exception( validated_data.get('thread').__class__ )
        message = validated_data.get('thread') \
            .send(message, attachment=message_att)

        return message
#   def create(self, validated_data):
#       validated_data['sender']
#   return Response( validated_data )


class MemberThreadMixin(EntitySerializerMixin):
    class Meta:
        model = MemberThread
        extra_kwargs = {
            'checked': {'read_only': True},
            'unchecked_count': {'read_only': True},
            'tuned': {'read_only': True},
            'is_admin': {'read_only': True},
            'thread': {'read_only': True}
        }
    member = serializers.HyperlinkedRelatedField(
        view_name='messaging:member-detail',
        queryset=Member.objects.all())

    # def create(self, validated_data):
    #   member = validated_data.get('member')
    #   thread = validated_data
    # def create(self):
    # thread = GenericRelatedField({
    #   Channel: serializers.HyperlinkedRelatedField(
    #       queryset=Channel.objects.all(),
    #       view_name='messaging:privatediscussion-detail'),
    #   PrivateDiscussion: serializers.HyperlinkedRelatedField(
    #       queryset=PrivateDiscussion.objects.all(),
    #       view_name='messaging:privatediscussion-detail'),
    # })


class MemberThreadSerializer(MemberThreadMixin, serializers.ModelSerializer):
    pass


class MemberChannelSerializer(MemberThreadMixin, serializers.ModelSerializer):
    pass
