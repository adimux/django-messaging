
def usertokenauthenticator_factory():
	"""
	`userauthenticator_factory` returns the Token Authenticator class (uses rest_framework.authentication.TokenAuthentication) 
	"""
	from .authenticators import UserTokenAuthenticator
	return UserTokenAuthenticator
